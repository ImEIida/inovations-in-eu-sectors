#Loading data
dd<-read.xls("data_model.xlsx", sheet= "Data")

#Loanding manifest and latent variables
se<-read.xls("data_model.xlsx", sheet= "MV")
le<-read.xls("data_model.xlsx", sheet= "LV")

#making matrixes MV&LV
sem<- as.matrix(se)
lem<- as.matrix(le)

#Model cod ..
model <- plsm(data = dd, strucmod = lem, measuremod = sem)
mvpairs(model = model, data = dd, LVs = "Size")
mvpairs(model = model, data = dd, LVs = "RnD")
mvpairs(model = model, data = dd, LVs = "Expenditure")
mvpairs(model = model, data = dd, LVs = "Cooperation")
mvpairs(model = model, data = dd, LVs = "Innovativeness")
mvpairs(model = model, data = dd, LVs = "Technical resourses")
mvpairs(model = model, data = dd, LVs = "Skilled labor")

model_rez <- sempls(model = model, data =dd, wscheme = "centroid",maxit=500)
pathDiagram(model_rez, file = "modelStructure", full = FALSE, edge.labels = "both", output.type = "graphics", digits = 2, graphics.fmt = "pdf")
model_rez
pC <- pathCoeff(model_rez)
print(pC, abbreviate = TRUE, minlength = 3)
tE <- totalEffects(model_rez)
print(tE, abbreviate = TRUE, minlength = 3)
plsWeights(model_rez)
plsLoadings(model_rez)
densityplot(model_rez, use = "residuals")

##### Boot..
set.seed(123)
model_rezBoot <- bootsempls(model_rez, nboot = 500, start = "ones", verbose = FALSE)
model_rezBoot
model_rezecsiBootsummary <- summary(model_rezBoot, type = "bca", level = 0.9)
model_rezecsiBootsummary

densityplot(model_rezBoot, pattern = "beta")
parallelplot(model_rezBoot, pattern = "beta", reflinesAt = c(0, 0.5), alpha = 0.3, type = "bca", main = "Path Coefficients\nof 500 bootstrap samples")
